import _ from 'lodash'
import K from 'kefir'
import 'purecss/build/pure.css'
import './screen.css'
import makeTimeline from './app/timeline'
import makeGeo, {
  locationsToMarkersPool,
  locationsToMarkers
} from './app/geo'
import searchBox from './app/search'
import { queryFilmLocations } from './app/api'
import {
  extractLocations,
  filmLocationToTimelineItem,
  prepareFilmLocation,
  uniqByTitle
} from './app/utils'
import voronoi from './app/voronoi'
import { locationToLatLng } from './app/places'
import tour from './app/tour'

// Main designation of this file is to build up a network of observables to make our app alive
// We'll instantiate stuff and connect it together

const filmLocationsFetcher = queryFilmLocations()
const filmLocations = K.fromEvents(
  filmLocationsFetcher, 'success',
  // some of the rows surprisingly have missing `locations` column, let's just drop them
  rows => rows.filter(x => x.locations).map(prepareFilmLocation)
)
const filmLocationsError = K.fromEvents(filmLocationsFetcher, 'error').map(() => false)

const searchQuery = searchBox({element: document.getElementById('search')}).map(x => x.toLowerCase())
const filmLocationsByQuery = filmLocations.combine(
  searchQuery,
  (xs, query) => {
    return query ? xs.filter(x => x.title.toLowerCase().includes(query)) : xs
  }
)

// REVIEW do we have rows with the same titles, but different years (film remakes)?
// REVIEW if so, uniqByTitle will screw things up
const films = filmLocationsByQuery.map(uniqByTitle)

const timelineItems = films.map(filmLocationToTimelineItem)
const timeline = makeTimeline({
  container: document.getElementById('timeline'),
  items: timelineItems
})

const dateRange = K.fromEvents(timeline, 'rangechanged')
const filmLocationsByQueryInRange = filmLocationsByQuery.combine(
  dateRange,
  () => timeline.itemsData.get(timeline.getVisibleItems()).map(x => x.data)
)

// FIXME move rendering out
const visibleLocations = K.fromEvents(
  timeline, 'select',
  ({items: [id]}) => {
    if (!id) return
    const {title, release_year: year, locations, fun_facts: facts} = timeline.itemsData.get(id).data
    const trivia = facts ? `<div><em>Trivia:</em><br/>${facts}</div>` : ''
    const popup = `<div>${title} (${year})</div>${trivia}`
    return locations.reduce((acc, x) => {
      acc[x] = popup
      return acc
    }, {})
  }
)
  .toProperty(() => null)
  .combine(filmLocationsByQueryInRange.map(extractLocations), (a, b) => a || b)

const geo = makeGeo()
const geoChange = K.fromEvents(geo, 'viewreset moveend').toProperty(() => null)

const voronoiDraw = voronoi(geo)
const voronoiData = visibleLocations.map(xs => _.size(xs) < 2 ? [] : _.map(xs, (titles, location) => ({
  density: _.isArray(titles) ? titles.length : 1,
  latitude: locationToLatLng(location)[0],
  longitude: locationToLatLng(location)[1]
})))
voronoiData.combine(geoChange, (a, _) => a).onValue(voronoiDraw)

const locations = filmLocations.map(extractLocations)
const markersPool = locationsToMarkersPool(geo, locations)
locationsToMarkers(visibleLocations, markersPool)

const geoReady = locations.map(() => true)
geoReady.merge(filmLocationsError).onValue((success) => {
  document.getElementById('loader').classList.add('hidden')
  if (success) {
    tour.start()
  } else {
    document.getElementById('load-error').classList.remove('hidden')
  }
})
