import { jsdom } from 'jsdom'
import chai from 'chai'
import sinonChai from 'sinon-chai'

chai.use(sinonChai)

const createLocalStorage = () => {
  const storage = {}
  return {
    getItem: (key) => {
      return storage[key]
    },
    setItem: (key, item) => {
      storage[key] = item
    },
    removeItem: (key) => {
      delete storage[key]
    }
  }
}

const exposedProperties = ['window', 'navigator', 'document']

global.document = jsdom('')
global.window = document.defaultView
global.localStorage = createLocalStorage()

Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property)
    global[property] = document.defaultView[property]
  }
})

global.navigator = {
  userAgent: 'node.js',
  platform: ''
}
