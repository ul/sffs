import { expect } from 'chai'
import { spy } from 'sinon'
import _ from 'lodash'
import K from 'kefir'
import {
  renderPopup,
  locationsToMarkers,
  locationsToMarkersPool
} from '../../app/geo'

describe('renderPopup', () => {
  it('should render location', () => {
    expect(renderPopup('TEST', '')).to.contain('TEST')
  })

  it('should render string content', () => {
    expect(renderPopup('', 'TEST')).to.contain('TEST')
  })

  it('should render array content', () => {
    const result = renderPopup('', ['TEST1', 'TEST2'])
    expect(result).to.contain('TEST1')
    expect(result).to.contain('TEST2')
  })
})

describe('locationsToMarkersPool', () => {
  it('should create marker with popup for every given location', () => {
    const locations = K.constant({
      a: 'A',
      b: 'B'
    })
    const markers = []
    const geo = {
      addLayer: (marker) => markers.push(marker)
    }
    const pool = locationsToMarkersPool(geo, locations)
    pool.onValue(pool => {
      expect(markers).to.have.length(2)
      expect(markers[0]._popup._content).to.eq(renderPopup('a', 'A'))
      expect(markers[1]._popup._content).to.eq(renderPopup('b', 'B'))
      expect(pool).to.deep.eq({
        a: markers[0],
        b: markers[1]
      })
    })
  })
})

describe('locationsToMarkers', () => {
  it('should show and hide markers from pool according to visible locations', () => {
    const visibleLocations = K.constant({a: 'VISIBLE'})
    const markers = {
      a: {
        _icon: {style: {}},
        _shadow: {style: {}},
        setPopupContent: spy()
      },
      b: {
        _icon: {style: {}},
        _shadow: {style: {}},
        closePopup: spy()
      }
    }
    const expectedMarkers = _.cloneDeep(markers)
    expectedMarkers.a._icon.style.display = 'block'
    expectedMarkers.a._shadow.style.display = 'block'
    expectedMarkers.b._icon.style.display = 'none'
    expectedMarkers.b._shadow.style.display = 'none'
    const markersPool = K.constant(markers)
    locationsToMarkers(visibleLocations, markersPool)
    expect(markers).to.deep.eq(expectedMarkers)
    expect(markers.a.setPopupContent).to.have.been.calledOnce
    expect(markers.b.closePopup).to.have.been.calledOnce
  })
})
