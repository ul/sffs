import { expect } from 'chai'
import { queryFilmLocations } from '../../app/api'

describe('queryFilmLocations', () => {
  it('should be subscribable', () => {
    expect(queryFilmLocations().on).to.be.a.function
  })
})
