import _ from 'lodash'
import * as d3 from 'd3'
import 'd3-voronoi'
import L from 'leaflet'

export default (map, id = 'overlay') => (points) => {
  d3.select('#' + id).remove()

  const bounds = map.getBounds()
  const topLeft = map.latLngToLayerPoint(bounds.getNorthWest())
  const existing = d3.set()
  const drawLimit = bounds.pad(0.4)

  const filteredPoints = points.filter(d => {
    var latlng = new L.LatLng(d.latitude, d.longitude)

    if (!drawLimit.contains(latlng)) return false

    const point = map.latLngToLayerPoint(latlng)

    const key = point.toString()
    if (existing.has(key)) return false
    existing.add(key)

    d.x = point.x
    d.y = point.y
    return true
  })

  d3.voronoi().x(d => d.x).y(d => d.y)(filteredPoints).polygons().forEach((p, i) => (filteredPoints[i].cell = _.compact(p)))

  const svg = d3.select(map.getPanes().overlayPane).append('svg')
    .attr('id', id)
    .attr('class', 'leaflet-zoom-hide')
    .style('width', map.getSize().x + 'px')
    .style('height', map.getSize().y + 'px')
    .style('margin-left', topLeft.x + 'px')
    .style('margin-top', topLeft.y + 'px')

  const g = svg.append('g')
    .attr('transform', 'translate(' + (-topLeft.x) + ',' + (-topLeft.y) + ')')

  const svgPoints = g.attr('class', 'points')
    .selectAll('g')
      .data(filteredPoints)
    .enter().append('g')
      .attr('class', 'point')

  const buildPathFromPoint = point => point.cell.length > 0 ? 'M' + point.cell.join('L') + 'Z' : ''

  svgPoints.append('path')
    .attr('class', 'point-cell')
    .attr('d', buildPathFromPoint)
    .style('opacity', d => 0.1 + 0.25 * Math.log(d.density))

  // svgPoints.append('circle')
  //   .attr('transform', d => 'translate(' + d.x + ',' + d.y + ')')
  //   .attr('r', 2)
}
