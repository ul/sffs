import { Tour } from 'tether-shepherd'
import 'tether-shepherd/dist/css/shepherd-theme-arrows.css'

const tour = new Tour({
  defaults: {
    classes: 'shepherd-element shepherd-open shepherd-theme-arrows',
    scrollTo: true,
    showCancelLink: true
  }
})

const buttons = [
  {
    text: 'Back',
    classes: 'shepherd-button-secondary',
    action: tour.back
  },
  {
    text: 'Next',
    action: tour.next
  }
]

tour.addStep('greeting', {
  text: '<p>Welcome to SFFLE!</p> <p>Sounds like soufflé... and is tasty no less!</p> <p>This is a little sweet visualization to find out which parts of San Fracisco you\'ve probably seen in films.<br/>We\'ve got a bunch of knobs to provide you with different slices of data, press "Next" to learn them.</p> <p>Feel free to try different options in action during this tour.</p>',
  attachTo: '#title bottom',
  buttons: [
    {
      text: 'Exit',
      classes: 'shepherd-button-secondary',
      action: tour.cancel
    },
    {
      text: 'Next',
      action: tour.next
    }
  ]
})

tour.addStep('timelineGeneral', {
  text: '<p>This is timeline.</p> <p>We have a lot of films here, and some years where super-productive!</p> <p>Scroll it to zoom in/out a time scale, drag back and forth to travel in time. Map will update to show locations for films which are inside date range.</p> <p>And don\'t forget to drag it up and down to discover new items when they stocks in a pile at fruity years.</p>',
  attachTo: '#timeline bottom',
  buttons
})

tour.addStep('mapGeneral', {
  text: '<p>This is map, you\'ve got it.</p> <p>But what about colored areas?</p> <p>They are about cinema attractiveness of that location. The more intense is color, the more films feature this location. Yes, Golden Bridge and Treasure Island are both cinema cliché (or stars, it depends on you stance) ;-)</p>',
  attachTo: '#map top',
  buttons
})

tour.addStep('timelineSelect', {
  text: '<p>You can select films by clicking on them. It will make map less crowdy, showing only locations related to this film. And you\'ll get a nice popup with film details.</p> <p>To deselect just click on the timeline background.</p>',
  attachTo: '#timeline bottom',
  buttons
})

tour.addStep('mapSelect', {
  text: '<p>Markers are clickable too! If no film is selected on timeline, it will show a list of films featuring that location. Otherwise, you\'ll have a chance to learn some fun facts if they present in our dataset for chosen location.</p>',
  attachTo: '#map bottom',
  buttons
})

tour.addStep('search', {
  text: '<p>And the ultimate way to get a narrow view is to filter films by title. Try to find you favorite film about SF!</p>',
  attachTo: 'form bottom',
  buttons: [
    {
      text: 'Back',
      classes: 'shepherd-button-secondary',
      action: tour.back
    },
    {
      text: 'Done',
      action: tour.next
    }
  ]
})

export default tour
