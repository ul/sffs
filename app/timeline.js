import { Timeline } from 'vis'
import 'vis/dist/vis.css'
import Drop from 'tether-drop'
import 'tether-drop/dist/css/drop-theme-arrows.css'

const yearOfMilliseconds = 365 * 24 * 60 * 60 * 1000

const resetItems = (timeline) => (xs) => {
  timeline.setItems(xs)
  timeline.fit()
}

// FIXME replace with proper templating or View library
const popupContent = ({title, release_year: year, locations, director}) => {
  return `<div>${title} (${year}) by ${director}</div>
  <div>Filmed in locations:
    <ul>
      ${locations.map(x => '<li>' + x + '</li>').join('')}
    </ul>
  </div>`
}

const defaultOptions = {
  maxHeight: 300,
  height: 300,
  autoResize: false,
  // clickToUse: true,
  showCurrentTime: false,
  showMajorLabels: false,
  timeAxis: {
    scale: 'year',
    step: 1
  },
  // type: 'point',
  zoomMax: 100 * yearOfMilliseconds,
  zoomMin: yearOfMilliseconds
}

const enablePopups = (timeline) => {
  let drop
  timeline.on('select', ({items: [id], event}) => {
    if (drop && drop.drop) drop.destroy()
    if (!id) return
    drop = new Drop({
      target: event.target,
      position: 'top center',
      content: popupContent(timeline.itemsData.get(id).data),
      classes: 'drop-theme-arrows'
    })
    window.requestAnimationFrame(() => drop.open())
  })
}

export default ({container, items, options = defaultOptions}) => {
  const timeline = new Timeline(container, [], options)
  items.onValue(resetItems(timeline))
  enablePopups(timeline)
  return timeline
}
