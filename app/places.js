// All feasible Geo-searching APIs have low free quota, thus we prepared and
// included statically this information
import places from '../places.json'

export const SAN_FRANCISCO = [37.783333, -122.416667]

// FIXME don't default to SF center, just drop locations w/o coords
export const locationToLatLng = (location) => {
  const p = places[location]
  return p ? [p.lat, p.lng] : SAN_FRANCISCO
}
