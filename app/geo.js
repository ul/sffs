import _ from 'lodash'
import L from 'leaflet'
import 'leaflet/dist/leaflet.css'
import { SAN_FRANCISCO, locationToLatLng } from './places'

export default ({center = SAN_FRANCISCO, zoom = 12} = {}) => {
  const geo = L.map('map').setView(center, zoom)
  // create the tile layer with correct attribution
  const osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
  const osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
  const osm = new L.TileLayer(osmUrl, {minZoom: 4, maxZoom: 19, attribution: osmAttrib})
  geo.addLayer(osm)

  return geo
}

// FIXME replace with proper templating or View library
export const renderPopup = (location, content) => `<div><strong>${location}</strong></div>` + (_.isArray(content) ? `<ul>${content.map(x => '<li>' + x + '</li>').join('')}</ul>` : `<div>${content}</div>`)

// REVIEW coords must come in locations
// REVIEW decoupling from popup rendering would be nice too
export const locationsToMarkersPool = (geo, locations) =>
  locations.map((locations) => _.mapValues(locations, (_, location) =>
    L.marker(locationToLatLng(location))
      .addTo(geo)
      .bindPopup(renderPopup(location, locations[location]))))

// FIXME better name
export const locationsToMarkers = (visibleLocations, markersPool) => visibleLocations
  .combine(markersPool)
  .onValue(([locations, markersPool]) => _.forEach(markersPool, (marker, location) => {
    if (locations[location]) {
      showMarker(marker, location, locations[location])
    } else {
      hideMarker(marker)
    }
  }))

// A little bit of exposed API violation to have better performance, as far as
// Leaflet doesn't provide efficient markers management tooling.

const showMarker = (marker, location, popup) => {
  marker._icon.style.display = 'block'
  marker._shadow.style.display = 'block'
  marker.setPopupContent(renderPopup(location, popup))
}

const hideMarker = (marker) => {
  marker._icon.style.display = 'none'
  marker._shadow.style.display = 'none'
  marker.closePopup()
}
