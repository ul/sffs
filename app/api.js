import { Consumer } from 'soda-js'

// Dataset is small enough to just fetch it at once and filter locally. In case
// of complex filtering or larger dataset we would better to produce
// parametrizable api observable.
export const queryFilmLocations = () => new Consumer('data.sfgov.org')
  .query()
  .withDataset('wwmu-gmzc')
  .getRows()
