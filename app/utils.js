import _ from 'lodash'

export const prepareFilmLocation = (x, id) => ({...x, id, date: new Date(+x.release_year, 0)})

export const uniqByTitle = xs => _.values(xs.reduce((acc, x) => {
  if (acc[x.title]) {
    acc[x.title].locations.push(x.locations)
  } else {
    acc[x.title] = {...x, locations: [x.locations]}
  }
  return acc
}, {})).map(x => ({...x, locations: _.uniq(_.compact(x.locations))}))

export const filmLocationToTimelineItem = xs => xs.map(x => ({
  id: x.id,
  content: x.title,
  start: x.date,
  data: x,
  style: `font-size: ${12 * (1 + 0.25 * Math.log(x.locations.length))}px;`
}))

export const extractLocations = (xs) => xs.reduce((acc, x) => {
  const content = `${x.title} (${x.release_year})`
  const locations = _.isArray(x.locations) ? x.locations : [x.locations]
  locations.forEach(location => {
    if (acc[location]) {
      acc[location].push(content)
    } else {
      acc[location] = [content]
    }
  })
  return acc
}, {})
