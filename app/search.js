import K from 'kefir'

export default ({element, debounce = 250}) => {
  return K.fromEvents(element, 'input', e => e.target.value).debounce(250).skipDuplicates().toProperty(() => element.value)
}
